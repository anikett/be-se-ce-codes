#include "graphics.h"
#include <stdio.h>

void drawCircle(int x1, int y1, int r) {
	int x=0,y=r;
	int pk = 1 - r;

	putpixel(x+x1,y+y1,GREEN);
	putpixel(x+x1,(y+y1)*-1,GREEN);

	while (x<y) {
		x+=1;
		if (pk <= 0) {
			pk += (2*x)+1;
		} else {
			y-=1;
			pk += (2*x) - (2*y) + 1;
		}
		putpixel(x+x1,y+y1,GREEN);
		putpixel(y+y1,x+x1,GREEN);
		putpixel(y+y1,(x*-1+x1),GREEN);
		putpixel(x+x1,(y*-1+y1),GREEN);
		putpixel((x*-1+x1),(y*-1+y1),GREEN);
		putpixel((y*-1+y1),(x*-1+x1),GREEN);
		putpixel((y*-1+y1),x+x1,GREEN);
		putpixel((x*-1+x1),y+y1,GREEN);
	}
}

int main() {
	int gd=DETECT, gm;
	initgraph(&gd,&gm,(char*) "");
	drawCircle(200,200,100);
	getch();
	closegraph();
	return 0;
}
