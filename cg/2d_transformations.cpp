#include "graphics.h"
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.142

int points_count;

float translation_matrix[3][3] = {
  {1, 0, 0},
  {0, 1, 0},
  {0, 0, 1}
};

float rotation_matrix[3][3] = {
  {0, 0, 0},
  {0, 0, 0},
  {0, 0, 1}
};

float scaling_matrix[3][3] = {
  {0,0,0},
  {0,0,0},
  {0,0,1}
};

float shearing_matrix[3][3] = {
  {1, 0, 0},
  {0, 1, 0},
  {0, 0, 1}
};

float **build_obj_matrix()
{
  float **objmatrix = (float **)malloc(sizeof(float *) * 3);
  for (int i = 0; i < 3; i++)
  {
    objmatrix[i] = (float *)malloc(sizeof(float) * points_count);
  }
  return objmatrix;
}

float **populate_obj_matrix(float **objmatrix)
{
  float x, y;
  printf("Enter the points in pairs. \n");
  for (int j = 0; j < points_count; j++)
  {
    scanf("%f %f", &x, &y);
    objmatrix[0][j] = x;
    objmatrix[1][j] = y;
    objmatrix[2][j] = 1;
  }
  return objmatrix;
}

float **multiply_matrix(float **obj_matrix, float transformation_matrix[][3])
{
  float sum;
  float **resultant_matrix = build_obj_matrix();
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < points_count; j++)
    {
      resultant_matrix[i][j] = 0;
      for (int k = 0; k < 3; k++)
      {
        resultant_matrix[i][j] += obj_matrix[k][j] * transformation_matrix[i][k];
      }
    }
  }
  return resultant_matrix;
}

int *to_plain(float **objmatrix)
{
  int *plain_mat = (int *)malloc(sizeof(int) * points_count * 2);
  int k = 0;
  for (int j = 0; j < points_count; j++)
  {
    plain_mat[k] = (int)objmatrix[0][j];
    plain_mat[++k] = (int)objmatrix[1][j];
    ++k;
  }
  plain_mat[k] = plain_mat[0];
  plain_mat[++k] = plain_mat[1];
  return plain_mat;
}

void print_mat(float **matrix)
{
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < points_count; j++)
    {
      printf("%f ", matrix[i][j]);
    }
    printf("\n");
  }
}

void print_menu()
{
  printf("1. Translation\n");
  printf("2. Rotation\n");
  printf("3. Scaling\n");
  printf("4. Shearing\n");
  printf("5. Exit\n");
  printf("Enter your choice: ");
}

int main()
{
  int choice;
  float **input_matrix, **resultant_matrix, xf, yf;
  int *print_matrix;
  while (true)
  {
    print_menu();
    scanf("%d", &choice);
    printf("Enter the number of points of the polygon: ");
    scanf("%d", &points_count);
    input_matrix = build_obj_matrix();
    input_matrix = populate_obj_matrix(input_matrix);
    switch (choice)
    {
    case 1:
      printf("Enter the X-translation factor: ");
      scanf("%f", &xf);
      printf("Enter the Y-transaltion factor: ");
      scanf("%f", &yf);
      translation_matrix[0][2] = xf;
      translation_matrix[1][2] = yf;
      resultant_matrix = multiply_matrix(input_matrix, translation_matrix);
      break;
    case 2:
      float angle, cosV, sinV;
      printf("Enter the angle to rotate at: ");
      scanf("%f", &angle);
      angle *= (PI/180);
      cosV = cos(angle);
      sinV = sin(angle);
      rotation_matrix[0][0] = cosV;
      rotation_matrix[0][1] = -sinV;
      rotation_matrix[1][0] = sinV;
      rotation_matrix[1][1] = cosV;
      resultant_matrix = multiply_matrix(input_matrix, rotation_matrix);
      break;
    case 3:
      printf("Enter the X-Scaling Factor: ");
      scanf("%f", &xf);
      printf("Enter the Y-Scaling Factor: ");
      scanf("%f", &yf);
      scaling_matrix[0][0] = xf;
      scaling_matrix[1][1] = yf;
      resultant_matrix = multiply_matrix(input_matrix, scaling_matrix);
      break;
    case 4:
      float factor;
      printf("1. For X-axis\n");
      printf("2. For Y-axis\n");
      scanf("%d", &choice);
      printf("Enter the shearing factor: ");
      scanf("%f", &factor);
      if(choice == 1) {
        shearing_matrix[1][0] = factor;
      } else if(choice == 2) {
        shearing_matrix[0][1] = factor;
      }
      resultant_matrix = multiply_matrix(input_matrix, shearing_matrix);
      break;
    case 5:
      break;
    default:
      printf("invalid choice");
    }
    int *print_matrix = to_plain(resultant_matrix);
    initwindow(1000, 900, "This Window");
    drawpoly(points_count + 1, print_matrix);
    print_matrix = to_plain(input_matrix);
    drawpoly(points_count + 1, print_matrix);
    getch();
    closegraph();
  }

  return 0;
}
