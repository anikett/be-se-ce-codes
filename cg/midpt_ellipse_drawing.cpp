#include <stdio.h>
#include <math.h>
#include "graphics.h"

void plotEllipse(int rx, int ry, int xc, int yc) {

    int x = 0;
    int y = ry;
    int dx = 2*(ry*ry)*x;
    int dy = 2*(rx*rx)*y;
    float p1 = (ry*ry) + (1/4) * (rx*rx) - (rx*rx)*ry;
    putpixel(x+xc,y+yc,RED);
    while(dx<dy) {
        if (p1 < 0) {
            x += 1;
            dx = 2*(ry*ry)*x;
            p1 += dx + (ry*ry);
        } else {
            x++;
            y--;
            dy = 2*(rx*rx)*y;
            dx = 2*(ry*ry)*x;
            p1 += dx + (ry*ry) - dy;
        }
        putpixel(x+xc,y+yc,RED);
        putpixel(x+xc,-y+yc,RED);
        putpixel(-x+xc,-y+yc,RED);
        putpixel(-x+xc,y+yc,RED);
    }

    float p2 = pow(ry, 2)*pow((x+(1/2)), 2)+pow(rx,2)*pow((y-1),2)-pow(rx,2)*pow(ry,2);
    while(y!=0) {
        if(p2>0) {
            y--;
            dy = 2*(rx*rx)*y;
            p2+=(rx*rx)-dy;
        } else {
            x++;
            y--;
            dy = 2*(rx*rx)*y;
            dx = 2*(ry*ry)*x;
            p2+=(rx*rx)-dy+dx;
        }
        putpixel(x+xc,y+yc,RED);
        putpixel(x+xc,-y+yc,RED);
        putpixel(-x+xc,-y+yc,RED);
        putpixel(-x+xc,y+yc,RED);
    }
}

int main() {
    int gd=DETECT, gm;
    initgraph(&gd, &gm, (char*)"");
    plotEllipse(50, 110, 200, 200);
    getch();
    closegraph();
    return 0;
}