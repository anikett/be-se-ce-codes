#include <stdio.h>
#include <math.h>
#include "graphics.h"

void koch(float x1, float y1, float x2, float y2, float it)
{
    float angle, i, x, y, x3, y3, x4, y4, xdiff, ydiff;

    // to radians
    angle = 60 * (3.142 / 180);

    xdiff = (x2 - x1) / 3.0;
    ydiff = (y2 - y1) / 3.0;

    x3 = x1 + xdiff;
    y3 = y1 + ydiff;

    x4 = x1 + 2 * xdiff;
    y4 = y1 + 2 * ydiff;

    x = x3 + (x4 - x3) * cos(angle) + (y4 - y3) * sin(angle);
    y = y3 - (x4 - x3) * sin(angle) + (y4 - y3) * cos(angle);

    if (it > 0)
    {
        koch(x1, y1, x3, y3, it - 1);
        delay(10);
        koch(x3, y3, x, y, it - 1);
        delay(10);
        koch(x, y, x4, y4, it - 1);
        delay(10);
        koch(x4, y4, x2, y2, it - 1);
        delay(10);
    }
    else
    {
        line(x1, y1, x3, y3);
        line(x3, y3, x, y);
        line(x, y, x4, y4);
        line(x4, y4, x2, y2);
    }
}
int main()
{
    int gd = DETECT, gm;
    float i, it, x1, y1, x2, y2;

    printf("Enter number of interation:");
    scanf("%f", &it);

    printf("Enter the value of x1 and y1:\n");
    scanf("%f", &x1);
    scanf("%f", &y1);

    printf("Enter the value of x2 and y2:\n");
    scanf("%f", &x2);
    scanf("%f", &y2);

    initgraph(&gd, &gm, "");
    for (i = 0; i < it; i++)
    {
        koch(x1, y1, x2, y2, it);
        getch();
    }
    closegraph();
    return 0;
}