## Running

### On Windows
- Need MSYS2 Installation
- Need 64-bit `WinBGIm` files — `libbgi.a`, `graphics.h`
- Use the `Makefile.win` with GNU Make
    ```cmd
    make -f Makefile.win program
    ```

## On Linux
- Need SDL_bgi installed
- Use `Makefile` with GNU Make
    ```bash
    make -f Makefile.nix program.o
    ```