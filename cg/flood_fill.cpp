#include "graphics.h"

void flood(int x, int y, int new_clr, int old_clr)
{
  if (getpixel(x, y) == old_clr) {
    putpixel(x, y, new_clr);
    flood(x + 1, y, new_clr, old_clr);
    flood(x - 1, y, new_clr, old_clr);
    flood(x, y + 1, new_clr, old_clr);
    flood(x, y - 1, new_clr, old_clr);
  }
}

int main() {
  int gd = DETECT, gm;
  initgraph(&gd,&gm,(char *)"");
  int x = 300, y = 300, radius = 75;
  setcolor(GREEN);
  circle(x,y,radius);
  int polyArr[] = {100, 100, 100, 200, 200, 200, 200, 100, 150, 50, 100, 100};
  drawpoly(6, polyArr);
  flood(x,y,GREEN,BLACK);
  flood(150, 150, GREEN, BLACK);
  getch();
  closegraph();
  return 0;
}
