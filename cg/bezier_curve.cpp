#include "graphics.h"
#include <math.h>

#define CONTROL_POINTS 4

void bezier(int x[CONTROL_POINTS], int y[CONTROL_POINTS])
{
    double t;
    double xt, yt;
    for (t = 0.0; t < 1.0; t += 0.0005)
    {
        xt = pow(1 - t, 3) * x[0] + 3 * t * pow(1 - t, 2) * x[1] + 3 * pow(t, 2) * (1 - t) * x[2] + pow(t, 3) * x[3];
        yt = yt = pow(1 - t, 3) * y[0] + 3 * t * pow(1 - t, 2) * y[1] + 3 * pow(t, 2) * (1 - t) * y[2] + pow(t, 3) * y[3];
        putpixel(xt, yt, WHITE);
    }
}

int main()
{
    int gd = DETECT, gm;
    int x[CONTROL_POINTS] = {100,200,300,400};
    int y[CONTROL_POINTS] = {100,400,400,100};
    initgraph(&gd, &gm, (char *)"");
    bezier(x, y);
    getch();
    closegraph();
    return 0;
}