#include <stdio.h>
#include "graphics.h"
#include <stdbool.h>

int x_min = 100, x_max = 300, y_max = 200, y_min = 50;

float max(float arr[], int n)
{
    int i;

    float max = arr[0];

    for (i = 1; i < n; i++)
        if (arr[i] > max)
            max = arr[i];

    return max;
}

float min(float arr[], int n) {
	int i;

	float min = arr[0];

	for(i=1; i<n; i++)
		if(arr[i]<min)
			min = arr[i];

	return min;
}

bool clip_line(int *x1, int *y1, int *x2, int *y2) {
	int delX = *x2-*x1;
	int delY = *y2-*y1;

	float p[5];
	p[1] = -delX;
	p[2] = delX;
	p[3] = -delY;
	p[4] = delY;

	float q[5];
	q[1] = *x1-x_min;
	q[2] = x_max-*x1;
	q[3] = *y1-y_min;
	q[4] = y_max-*y1;

	bool toDraw = false;
	for(int i=1; i<5; i++) {
		if(p[i]!=0&&q[i]!=0) {
			toDraw = true;
		}
	}

	if(!toDraw) {
		return false;
	}

	float r[5] = {0};
	float u1_arr[5] = {0};
	float u2_arr[5] = {1,1,1,1,1};

	for(int i=1; i<5; i++) {
		if(p[i]!=0) {
			r[i] = (q[i]/p[i]);
			if(p[i]<0) {
				u1_arr[i] = r[i];
			} else if(p[i]>0) {
				u2_arr[i] = r[i];
			}
		}
	}

	float u1, u2;
	u1 = max(u1_arr, 5);
	u2 = min(u2_arr, 5);

	*x1 = *x1+u1*delX;
	*x2 = *x1+u2*delX;
	*y1 = *y1+u1*delY;
	*y2 = *y1+u2*delY;

	return true;
}

int main() {
	int gd=DETECT, gm, x1, y1, x2, y2;
	x1=150,y1=40,x2=200,y2=80;

	initgraph(&gd,&gm,(char*)"");

	rectangle(x_min, y_max, x_max, y_min);
	line(x1,y1,x2,y2);

	getchar();
	closegraph();

	initgraph(&gd,&gm,(char*)"");

	clip_line(&x1,&y1,&x2,&y2);
	rectangle(x_min, y_max, x_max, y_min);
	line(x1,y1,x2,y2);

	getch();
	closegraph();

	return 0;
}