#include "graphics.h"

void boundaryFill4(int x, int y, int fill_color, int boundary_color)
{
    if (getpixel(x, y) != boundary_color && getpixel(x, y) != fill_color)
    {
        putpixel(x, y, fill_color);
        boundaryFill4(x + 1, y, fill_color, boundary_color);
        boundaryFill4(x, y + 1, fill_color, boundary_color);
        boundaryFill4(x - 1, y, fill_color, boundary_color);
        boundaryFill4(x, y - 1, fill_color, boundary_color);
    }
}

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, (char *)"");
    int x = 150, y = 150, radius = 100;
    circle(x, y, radius);
    int polyArr[] = {320, 150, 400, 250,
                     250, 350, 320, 150};
    drawpoly(4, polyArr);
    boundaryFill4(x, y, RED, WHITE);
    boundaryFill4(390, 245, RED, WHITE);
    getch();
    closegraph();
    return 0;
}
