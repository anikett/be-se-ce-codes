#include "graphics.h"
#include <stdio.h>

void plotBresLine(int x1, int y1, int x2, int y2)
{
	int delX = x2 - x1;
	int delY = y2 - y1;
	int twoDelY = 2 * delY;
	int twoDelX = 2 * delX;
	int pk = twoDelY - delX;

	int xk = x1, yk = y1;
	putpixel(xk, yk, GREEN);
	for (int i = 0; i < delX; i++)
	{
		if (pk < 0)
		{
			pk += twoDelY;
		}
		else
		{
			pk += twoDelY - twoDelX;
			yk++;
		}
		xk++;
		putpixel(xk, yk, GREEN);
	}
}

int main()
{
	int gd = DETECT, gm;
	initgraph(&gd, &gm, (char *)"");
	plotBresLine(40, 40, 120, 120);
	getch();
	closegraph();
	return 0;
}
