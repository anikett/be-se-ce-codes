#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int data;
    struct Node* next_node;
} Node;

Node *HEAD = NULL;

Node *create_node(int data)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next_node = NULL;
    return newNode;
}

void push(Node *newNode)
{
    if (HEAD == NULL)
    {
        HEAD = newNode;
    }
    else
    {
        newNode->next_node = HEAD;
        HEAD=newNode;
    }
};

void pop() {
    if(HEAD==NULL) {
        printf("The stack is empty.\n");
    } else {
        Node *temp = HEAD->next_node;
        free(HEAD);
        HEAD = temp;
    }
}

void peek() {
    if(HEAD==NULL) {
        printf("The stack is empty.\n");
    } else {
        printf("%d", HEAD->data);
    }
}

void print_menu() {
    printf("1. Insert\n");
    printf("2. Pop\n");
    printf("3. Peek\n");
    printf("4. Exit\n");
    printf("Enter your choice: ");
}

int main()
{
    int choice, temp;
    Node* tempNode;
    while(1) {
        print_menu();
        scanf("%d", &choice);
        switch(choice) {
            case 1:
                printf("Enter the element: ");
                scanf("%d", &temp);
                tempNode = create_node(temp);
                push(tempNode);
                break;
            case 2:
                pop();
                break;
            case 3:
                peek();
                break;
            case 4:
                exit(0);
                break;
            default:
                printf("invalid choice");
        }
    }
    return 0;
}