#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct Node
{
    int data;
    struct Node *next;
} Node;

Node* HEAD = NULL;

bool isEmpty() {
    if(HEAD==NULL)
        return true;
    return false;
}

Node* create_node(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

void insert_at_beg(Node* newNode) {
    if(isEmpty()) {
        HEAD = newNode;
        HEAD->next = newNode;
    } else {
        Node* lastNode = HEAD;
        while(lastNode->next!=HEAD) {
            lastNode = lastNode->next;
        }
        newNode->next = HEAD;
        HEAD = newNode;
        lastNode->next = HEAD;
    }
}

void insert_at_pos(Node* newNode, int search_data) {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }

    Node* temp_l = HEAD;
    while(temp_l->data!=search_data) {
        temp_l = temp_l->next;
        if(temp_l->next==HEAD) {
            break;
        }
    }
    Node* temp_r = temp_l->next;
    newNode->next=temp_r;
    temp_l->next = newNode;
}

void insert_at_end(Node* newNode) {
    if(isEmpty()) {
        HEAD = newNode;
        newNode->next = HEAD;
    } else {
        Node* lastNode = HEAD;
        while(lastNode->next!=HEAD) {
            lastNode = lastNode->next;
        }

        lastNode->next = newNode;
        newNode->next = HEAD;
    }
}

void delete_at_beg() {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }

    Node* temp = HEAD;
    if(temp->next==HEAD) {
        free(HEAD);
        HEAD=NULL;
    } else {
        temp = temp->next;
        Node* lastNode = HEAD;
        while(lastNode->next!=HEAD) {
            lastNode = lastNode->next;
        }
        free(HEAD);
        HEAD=temp;
        lastNode->next = HEAD;
    }
}

void delete_at_end() {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }

    Node* penultimateNode = HEAD;

    while(penultimateNode->next->next!=HEAD) {
        penultimateNode = penultimateNode->next;
    }

    // handle single node case
    if(penultimateNode==HEAD) {
        free(HEAD);
        HEAD=NULL;
    } else {
        free(penultimateNode->next);
        penultimateNode->next = HEAD;
    }
}

void delete_at_pos(int search_data) {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }

    Node* temp_l = HEAD;
    while(temp_l->next->data != search_data) {
        temp_l = temp_l->next;
        if(temp_l->next==HEAD) {
            printf("Data not found.\n");
            return;
        }
    }

    Node* temp_r = temp_l->next->next;
    // handle single node case
    if(temp_l==temp_l->next) {
        free(HEAD);
        HEAD=NULL;
    } else {
        free(temp_l->next);
        temp_l->next = temp_r;
    }
}

void display() {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }
    Node* temp = HEAD;
    printf("%d -> ", temp->data);
    while(temp->next!=HEAD) {
        temp = temp->next;
        printf("%d -> ", temp->data);
    }
    printf("HEAD\n\n");
}

void print_menu() {
    printf("1. Insert at beginning\n");
    printf("2. Insert at end\n");
    printf("3. Insert at a position\n");
    printf("4. Delete from beginning\n");
    printf("5. Delete from end\n");
    printf("6. Delte at position\n");
    printf("7. Display List\n");
    printf("8. Exit\n");
    printf("Choose the operation to perform: ");
}

int main() {
    int choice, inval, search_data;
    Node* newNode;

    while(true) {
        print_menu();
        scanf("%d", &choice);
        switch(choice) {
        case 1:
            printf("Enter the data: ");
            scanf("%d", &inval);
            newNode = create_node(inval);
            insert_at_beg(newNode);
            break;
        case 2:
            printf("Enter the data: ");
            scanf("%d", &inval);
            newNode = create_node(inval);
            insert_at_end(newNode);
            break;
        case 3:
            printf("Enter the data to insert after: ");
            scanf("%d", &search_data);
            printf("Enter the data to insert: ");
            scanf("%d", &inval);
            newNode = create_node(inval);
            insert_at_pos(newNode, search_data);
            break;
        case 4:
            delete_at_beg();
            break;
        case 5:
            delete_at_end();
            break;
        case 6:
            printf("Enter the data to delete: ");
            scanf("%d", &search_data);
            delete_at_pos(search_data);
            break;
        case 7:
            display();
            break;
        case 8:
            exit(0);
        default:
            printf("Invalid choice.\n\n");
        }
    }
    return 0;
}