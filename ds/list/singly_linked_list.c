#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node* next;
} Node;

Node* HEAD = NULL;

bool isEmpty() {
    if(HEAD==NULL)
        return true;
    return false;
}

Node* create_node(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

void insert_at_beg(Node* newNode) {
    if(isEmpty()) {
        HEAD=newNode;
    } else {
        newNode->next = HEAD;
        HEAD=newNode;
    }
};

void insert_at_end(Node* newNode) {
    if(isEmpty()) {
        HEAD=newNode;
        HEAD->next = HEAD;
    } else {
        Node* lastNode = HEAD;
        while(lastNode->next!=NULL) {
            lastNode = lastNode->next;
        }
        lastNode->next = newNode;
    }
};

void insert_at_pos(Node* newNode, int search_data) {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }

    Node* temp_l = HEAD;
    while(temp_l->data!=search_data) {
        temp_l = temp_l->next;
        if(temp_l->next == HEAD) {
            break;
        }
    }

    Node* temp_r = temp_l->next;
    newNode->next = temp_r;
    temp_l->next = newNode;
};

void delete_at_beg() {
    if(isEmpty()) {
        printf("The list is empty.");
        exit(1);
    }

};

void delete_at_end();

void delete_at_pos(int search_data);

int main() {

    return 0;
}