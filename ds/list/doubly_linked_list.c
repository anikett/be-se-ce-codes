#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
} Node;

Node* HEAD = NULL;

bool isEmpty() {
    if(HEAD==NULL)
        return true;
    return false;
}

Node* create_newnode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;
}

void insert_at_beg(Node* newNode) {
    if(isEmpty())  {
        HEAD = newNode;
    } else {
        newNode->next = HEAD;
        HEAD->prev = newNode;
        HEAD=newNode;
    }
}

void insert_at_end(Node* newNode) {
    if(isEmpty()) {
        HEAD = newNode;
    } else {
        Node* lastNode = HEAD;
        while(lastNode->next != NULL) {
            lastNode = lastNode->next;
        }
        newNode->prev = lastNode;
        lastNode->next = newNode;
    }
}

void insert_at_pos(Node* newNode, int search_data) {
    if(isEmpty()) {
        printf("List is empty.\n");
        exit(1);
    }

    Node* temp_l = HEAD;

    while(temp_l->next != NULL && temp_l->data != search_data) {
        temp_l = temp_l->next;
    }

    if(!(temp_l->data == search_data)) {
        printf("Search data not found.\n");
    }

    Node* temp_r = temp_l->next;
    newNode->prev = temp_l;
    newNode->next = temp_r;
    temp_l->next = newNode;
    temp_r->prev = newNode;
}

void delete_at_beg() {
    if(isEmpty()) {
        printf("The list is empty.\n");
        exit(1);
    }

    Node* temp = HEAD->next;
    free(HEAD);
    if(temp == NULL) {
        HEAD=NULL;
    } else {
        HEAD=temp;
        HEAD->prev=NULL;
    }
}

void delete_at_end() {
    if(isEmpty()) {
        printf("The list is empty.\n");
        exit(1);
    }

    Node* temp = HEAD;
    while(temp->next != NULL) {
        temp = temp->next;
    }
    temp->prev->next = NULL;
    free(temp);
}

void delete_at_pos(int search_data) {
    if(isEmpty()) {
        printf("The list is empty.\n");
        exit(1);
    }

    Node* temp = HEAD;

    while(temp->next!=NULL && temp->data != search_data) {
        temp = temp->next;
    }

    if(temp->data!=search_data) {
        printf("Data not found.\n");
        exit(1);
    }
    temp->prev->next = temp->next;
    temp->next->prev = temp->prev;
    free(temp);
}

void display() {
    if(isEmpty()) {
        printf("The list is empty.\n");
        exit(1);
    }

    Node* temp = HEAD;
    printf("%d ->", temp->data);
    while(temp->next != NULL) {
        temp = temp->next;
        printf("<- %d ->", temp->data);
    }
    printf("NULL\n");
}

int main() {
    insert_at_beg(create_newnode(5));
    insert_at_end(create_newnode(9));
    insert_at_beg(create_newnode(7));
    insert_at_pos(create_newnode(10), 7);
    display();
    delete_at_pos(5);
    delete_at_end();
    delete_at_beg();
    display();

    return 0;
}