#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
	int data;
	struct node* left;
	struct node* right;
} Node;

Node* ROOT = NULL;

int minvalue(Node* ROOT) {
	int min = ROOT->data;
	while(ROOT->left != NULL) {
		min = ROOT->left->data;
		ROOT = ROOT->left;
	}
	return min;
}


void insert(Node* ROOT, Node* newNode) {
	if(newNode->data < ROOT->data) {
		if(ROOT->left!=NULL) {
			insert(ROOT->left, newNode);
		} else {
			ROOT->left = newNode;
		}
	}

	if(newNode->data > ROOT->data) {
		if(ROOT->right != NULL)
			insert(ROOT->right, newNode);
		else
			ROOT->right = newNode;
	}
}

Node* delete_node(Node* ROOT, int key) {
	Node* temp;

	if(ROOT==NULL)
		return ROOT;

	if(key<ROOT->data) {
		ROOT->left = delete_node(ROOT->left, key);
	} else if(key>ROOT->data) {
		ROOT->right = delete_node(ROOT->right, key);
	} else {
		if(ROOT->left == NULL) {
			temp = ROOT->right;
			free(ROOT);
			return temp;
		} else if(ROOT->right==NULL) {
			temp = ROOT->left;
			free(ROOT);
			return temp;
		} else {
			ROOT->data = minvalue(ROOT->right);
			ROOT->right = delete_node(ROOT->right, ROOT->data);
		}
	}

	return ROOT;
}

Node* create_node(int data) {
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->data = data;
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;
}

void inorder(Node* ROOT) {
	Node* temp = ROOT;
	if(temp!=NULL) {
		inorder(temp->left);
		printf("%d ", temp->data);
		inorder(temp->right);
	}
	printf("\n");
}

void preorder(Node* ROOT) {
	Node* temp = ROOT;
	if(temp!=NULL) {
		printf("%d", temp->data);
		preorder(temp->left);
		preorder(temp->right);
	}
	printf("\n");
}

void postorder(Node* ROOT) {
	Node* temp = ROOT;
	if(temp!=NULL) {
		postorder(temp->left);
		postorder(temp->right);
		printf("%d ", temp->data);
	}
	printf("\n");
}

void print_menu() {
	printf("BST Operations: \n");
	printf("1. Construct Tree\n");
	printf("2. Inorder\n");
	printf("3. Postorder\n");
	printf("4. Preorder\n");
	printf("5. Delete\n");
	printf("6. Exit\n");
	printf("Enter your choice: ");
}

int main() {
	int ch, data, totalNodes;
	Node* newNode;
	while(true) {
		print_menu();
		scanf("%d", &ch);
		printf("\n");
		switch(ch) {
		case 1:
			printf("Enter no. of elements to be inserted in the tree: ");
			scanf("%d", &totalNodes);
			for(int i=0; i<totalNodes; i++) {
				printf("Enter data: ");
				scanf("%d", &data);
				newNode = create_node(data);
				if(ROOT==NULL)
					ROOT=newNode;
				else
					insert(ROOT, newNode);
			}
			break;
		case 2:
			inorder(ROOT);
			break;
		case 3:
			postorder(ROOT);
			break;
		case 4:
			preorder(ROOT);
			break;
		case 5:
			printf("Enter the node to delete: ");
			scanf("%d", &data);
			ROOT = delete_node(ROOT, data);
			break;
		case 6:
			exit(0);
		}
	}
}
