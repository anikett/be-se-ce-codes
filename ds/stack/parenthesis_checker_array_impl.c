#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 50

int top = -1;
char arr[MAX];

int isFull() {
	if (top==MAX-1) {
		return 1;
	}

	return 0;
};

int isEmpty() {
	if (top==-1){
		return 1;
	}
	return 0;
};

void push(char in) {
	if (isFull()) {
		printf("overflow\n");
    exit(1);
	}
	top += 1;
	arr[top] = in;
};

void pop() {
	if(isEmpty()) {
		printf("underflow\n");
		exit(1);
	}
	top-=1;
};

char peek() {
	if (isEmpty()) {
		printf("no element on stack\n");
    exit(1);
	}
  return arr[top];
};

int main() {
  char expr[MAX];
  printf("Enter the expression to check: ");
  gets(expr);
  for(int i=0; i<strlen(expr); i++) {
    if (expr[i] == '(') {
      push(expr[i]);
    } else if (expr[i] == ')') {
      pop();
    }
  }

  if(!isEmpty()) {
    printf("The expression is not well-formed.\n");
    return 1;
  }

  printf("Good! The expression is well-formed.\n");
  return 0;
}
