#include <stdio.h>

int maxArrL = 50;

int top = -1;
int arr[50];

int isFull() {
	if (top==maxArrL-1) {
		return 1;
	}

	return 0;
};

int isEmpty() {
	if (top==-1){
		return 1;
	}
	return 0;
};

void push(int in) {
	if (isFull()) {
		printf("overflow\n");
		return;
	}
	top += 1;
	arr[top] = in;
};

void pop() {
	if(isEmpty()) {
		printf("underflow\n");
		return;
	}
	top-=1;
};

void search(int query) {
	for(int i=top; i>0; i--){
		if (arr[i] == query){
			printf("found\n");
			return;
		}
	}
	printf("not found\n");
	return;
};

void peek() {
	if (isEmpty()) {
		printf("no element on stack\n");
		return;
	}
	printf("%d\n", arr[top]);
};

void print_choice_menu() {
	printf("1. Push\n");
	printf("2. Pop\n");
	printf("3. Search\n");
	printf("4. Peek\n");
	printf("5. Exit\n");
	printf("Choose your opn: ");
}

void get_elem(int* elem) {
	printf("Enter the element: ");
	scanf("%d", elem);
}

int main() {
	int choice, elem;
	while (1) {
		print_choice_menu();
	 	scanf("%d", &choice);
		switch (choice) {
			case 1:
				get_elem(&elem);
				push(elem);
				break;
			case 2:
				pop();
				break;
			case 3:
				get_elem(&elem);
				search(elem);
				break;
			case 4:
				peek();
				break;
			case 5:
				return 0;
			default:
				printf("invalid choice");
		}
	}

	return 0;
}
