#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int maxArrL = 50;

int top = -1;
char arr[50];
int openingBrsOnStack = 0;

int isFull() {
	if (top==maxArrL-1) {
		return 1;
	}

	return 0;
};

int isEmpty() {
	if (top==-1){
		return 1;
	}

	return 0;
};

void push(char in) {
	if (isFull()) {
		printf("overflow\n");
		exit(1);
	}
	top += 1;
	arr[top] = in;
};

char pop() {
	if(isEmpty()) {
		printf("underflow\n");
		exit(1);
	}
	top-=1;
	return arr[top+1];
};

char peek() {
	if (isEmpty()) {
		printf("no element on stack\n");
		exit(1);
	}
	return arr[top];
};

int priority(char op) {
    if(op=='^') {
      return 2;
    }

    if(op == '*' || op == '/') {
        return 1;
    }

    if(op == '+' || op == '-') {
        return 0;
    }

    return 0;
}

int isOperator(char in) {
	if (in == '+' || in == '-' || in == '*' || in == '/' || in == '^'|| in == '(' || in == ')')
		return 1;

	return 0;
}

int main() {
    char expr[50];
    printf("Enter the expression to convert: ");
    gets(expr);
    char el;
    for(int i=0; i < strlen(expr); i++) {
        el = expr[i];
        if(isalpha(el)) {
            printf("%c", el);
        } else if (isOperator(el)) {
            if(el == '('){
                push(el);
                openingBrsOnStack += 1;
            }
            else if (el == ')') {
                while(peek() != '(') {
                    printf("%c", pop());
                }
                // discard the opening parenthese
                pop();
                openingBrsOnStack -= 1;
            }
            else if(isEmpty() || openingBrsOnStack > 0 || priority(el) > priority(peek())) {
                push(el);
            }
            else {
                while(!isEmpty() && (priority(el) <= priority(peek()))) {
                    printf("%c", pop());
                }
                push(el);
            }
        }
    }

    while(!isEmpty()) {
        printf("%c", pop());
    }
    printf("\n");
    return 0;
}
