#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int maxArrL = 50;

int top = -1;
double arr[50];

int isFull()
{
	if (top == maxArrL - 1)
	{
		return 1;
	}

	return 0;
};

int isEmpty()
{
	if (top == -1)
	{
		return 1;
	}

	return 0;
};

void push(double in)
{
	if (isFull())
	{
		printf("overflow\n");
		exit(1);
	}
	top += 1;
	arr[top] = in;
};

double pop()
{
	if (isEmpty())
	{
		printf("underflow\n");
		exit(1);
	}
	top -= 1;
	return arr[top + 1];
};

int isOperator(char in)
{
	if (in == '+' || in == '-' || in == '*' || in == '/' || in == '^')
		return 1;

	return 0;
}

double operate(double i1, double i2, char op)
{
	switch (op)
	{
	case '+':
		return i1 + i2;
		break;
	case '-':
		return i1 - i2;
		break;
	case '/':
		return i1 / i2;
		break;
	case '*':
		return i1 * i2;
		break;
	case '^':
		return pow(i1, i2);
		break;
	default:
		exit(1);
	}
}

int main()
{
	double i1, i2, res;
	char el;
	char expr[50];
	printf("Enter the expression to evaluate: ");
	gets(expr);

	for (int i = 0; i < strlen(expr); i++)
	{
		el = expr[i];

		if (isdigit(el))
		{
			push((double)atoi(&el));
		}
		else if (isOperator(el))
		{
			i2 = pop();
			i1 = pop();
			res = operate(i1, i2, el);
			push(res);
		}
	}

	printf("The result is: %f\n", res);

	return 0;
}
