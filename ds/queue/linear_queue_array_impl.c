#include <stdio.h>
#include <stdlib.h>

#define MAX 3

void enqueue(int);
int dequeue();
int isFull();
int isEmpty();
void display();

int arr[MAX];
int front = -1;
int rear = -1;

int main() {
	int choice, elem;

	printf("1. Enqueue\n");
	printf("2. Dequeue\n");
	printf("3. Display\n");
	printf("4. Exit\n");
	
	while(choice != 4) {
		printf("Enter your choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				printf("Enter the element: ");
				scanf("%d", &elem);
				enqueue(elem);
				break;
			case 2:
				dequeue();
				break;
			case 3:
				display();
				break;
		}
	}
	
	return 0;
}

int isEmpty() {
	if(front == -1 && rear == -1) {
		return 1;
	}
	return 0;
}

int isFull() {
	if(rear == MAX-1) {
		return 1;
	}
	return 0;
}

void enqueue(int in) {
	if(isFull()) {
		printf("overflow");
		exit(1);
	}
	
	if(isEmpty()) {
		front++;
	}
	
	rear++;
	arr[rear] = in;
}

int dequeue() {
	if(isEmpty()) {
		printf("underflow");
		exit(1);
	}
	
	int data = arr[front];
	
	if(front == rear) {
		front = rear = -1;
	} else {
		front++;
	}
	
	return data;
}

void display() {
	printf("[ ");
	if(!isEmpty()) {
		for(int i = front; i <= rear; i++) {
			printf("%d ", arr[i]);
		}
	}
	printf("]\n");
}
