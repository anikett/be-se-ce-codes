#include <stdio.h>
#include <stdlib.h>

#define MAX 3

void enqueue(int);
int dequeue();
int isFull();
int isEmpty();
void display();

int arr[MAX];
int front = -1;
int rear = -1;

int main() {
	int choice, elem;

	printf("1. Enqueue\n");
	printf("2. Dequeue\n");
	printf("3. Display\n");
	printf("4. Exit\n");
	
	while(choice != 4) {
		printf("Enter your choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				printf("Enter the element: ");
				scanf("%d", &elem);
				enqueue(elem);
				break;
			case 2:
				dequeue();
				break;
			case 3:
				display();
				break;
		}
	}
	
	return 0;
}

int isEmpty() {
	if(front == -1 && rear == -1) {
		return 1;
	}
	return 0;
}

int isFull() {
	if((rear == MAX-1 && front == 0) || rear+1 == front) {
		return 1;
	}
	return 0;
}

void enqueue(int in) {
	if(isFull()) {
		printf("overflow");
		exit(1);
	}
	
	if(isEmpty()) {
		front++;
	}
	
	if(rear == MAX-1) {
		rear = 0;
	} else {
		rear++;
	}

	arr[rear] = in;
}

int dequeue() {
	int data;
	
	if(isEmpty()) {
		printf("underflow");
		exit(1);
	}

	data = arr[front];
	
	if(front == rear) {
		front = rear = -1;
	} else {
		if(front == MAX-1) {
			front = 0;
		} else {
			front++;
		}
	}
	
	return data;
}

void display() {
	int i;
	printf("[ ");
	if(!isEmpty()) {
		if(front<rear) {
			for(i = front; i <= rear; i++) {
				printf("%d ", arr[i]);
			}
		} else {
			i = front;
			do{
				printf("%d ", arr[i]);
				if(i == MAX-1) {
					i = 0;
				} else {
					i++;
				}
			} while(i!=rear+1);
		}
	}

	printf("]\n");
}
